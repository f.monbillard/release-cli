# Download `release-cli` binaries

Multiple `release-cli` binaries are available to download from the generic package registry.
You can browse the available versions in the
[generic package registry](https://gitlab.com/gitlab-org/release-cli/-/packages) for this project.

**NOTE**: The generic package registry may show multiple files for the same version. Review the created
time to make sure the file is the latest one if browsing the files in the UI.

List of compiled binaries available to download:

- release-cli-darwin-amd64
- release-cli-freebsd-386
- release-cli-freebsd-amd64
- release-cli-freebsd-arm
- release-cli-linux-386
- release-cli-linux-amd64
- release-cli-linux-arm
- release-cli-linux-arm64
- release-cli-linux-ppc64le
- release-cli-windows-386.exe
- release-cli-windows-amd64.exe

The updated list can also be found in the [`.build.yml` CI configuration file](../../../.gitlab/ci/build.yml)

## Using the latest binary

You can use the `latest` version of a binary that matches the
[latest release](https://gitlab.com/gitlab-org/release-cli/-/releases/) for this CLI.
The binary file can be downloaded using the API without the need of authentication.

```shell
$ BINARY_NAME=release-cli-darwin-amd64
$ curl "https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/latest/$BINARY_NAME" --output release-cli
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 6876k  100 6876k    0     0  5143k      0  0:00:01  0:00:01 --:--:-- 5143k

$ chmod +x ./release-cli
$ ./release-cli -v
release-cli version 0.8.0~beta.0.g6e691f9
```
